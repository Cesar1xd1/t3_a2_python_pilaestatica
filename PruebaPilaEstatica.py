'''
Created on 16 oct. 2020

@author: Cesar
'''

class Correcion:
    
    def validacion(self):
        while True:
            try:
                r = int(input())
            except:
                print("Solamente numeros: ")
            else:
                if r>0:
                    break
                else:
                    print("Solamente enteros positivos, intente de nuevo: ")
        return r
    def validacionC(self):
        while True:
            try:
                r = int(input())
            except:
                print("Solamente numeros: ")
            else:
                if r>4:
                    break
                else:
                    print("Solo numeros superiores o iguales a 5: ")
        return r

class Pelicula:
    def __init__(self, titulo, genero):
        if titulo is None:
            self.__titulo = str("")
        else:
            self.__titulo = titulo
        
        if genero is None:
            self.__genero = str("")
        else:
            self.__genero = genero
    
    def getTitulo(self):
        return self.__titulo
    def setTitulo(self, titulo):
        self.__titulo=titulo
    
    def getGenero(self):
        return self.__genero
    def setGenero(self, genero):
        self.__genero=genero
    
    def __str__(self):
        return ("Titulo=" + self.getTitulo() + ", genero=" + self.getGenero() + "]")

class RentaPeliculas:
    def crear(self):
        return []
    def apilar(self, pila, pelicula):
        pila.append(pelicula)
        return pila
    def vacia(self, pila):
        return (len(pila)==0)
    def desapilar(self, pila):
        if(self.vacia(RentaPeliculas, pila)):
            print("La pila esta vacia")
        else:
            pila.pop()
        return pila
    def cima(self, pila):
        if(self.vacia(RentaPeliculas, pila)):
            print("La pila esta vacia")
            return -1
        else:
            return pila[-1]
    def rentar(self, pila):
        print("Rentando pelicula ...")
        if(self.vacia(RentaPeliculas, pila)):
            print("No se pudo rentar la pelicula")
            return None
        else:
            return self.cima(RentaPeliculas, pila)
    def devolver(self, pila, pelicula):
        print("Regresando pelicula ...")
        self.apilar(RentaPeliculas, pila, pelicula)
        return pila
    
class ImplementacionPilaEstatica:
    def __init__(self, n):
        self.peliculas = ([None]*int(n))
        self.puntero = -1
            

    def llena(self):
        return (self.puntero==(len(self.peliculas)-1))
    def vacia(self):
        return (self.puntero==(-1))
    def rentar(self):
        if(self.vacia()):
            print("La pila esta vacia")
            return None
        else:
            peliculastmp=([None]*(self.puntero+1))
            for i in range(len(peliculastmp)):
                peliculastmp[i]=self.peliculas[i]
            ret = RentaPeliculas.rentar(RentaPeliculas, peliculastmp)
            peliculasret = RentaPeliculas.desapilar(RentaPeliculas, peliculastmp)
            for i in range(len(peliculasret)):
                self.peliculas[i]=peliculasret[i]
            self.peliculas[self.puntero]=None
            self.puntero-=1
            return ret
    def devolver(self, pelicula):
        if(self.llena()):
            print("La pila estatica esta llena")
        else:
            peliculastmp=([None]*(self.puntero+1))
            for i in range(len(peliculastmp)):
                peliculastmp[i]=self.peliculas[i]
            peliculasret = RentaPeliculas.devolver(RentaPeliculas, peliculastmp, pelicula)
            for i in range(len(peliculasret)):
                self.peliculas[i]=peliculasret[i]
            self.puntero+=1
    def cantidad(self):
        print("Cantidad de peliculas disponibles: "+str(self.puntero+1))
    
    def mostrar(self):
        for i in self.peliculas:
            print(i)

rentada = Pelicula(None,None)
ipe = ImplementacionPilaEstatica(5)

while True:
    print("Digtie 1 para gargar la Base de Datos de peliculas")
    print("Digite 2 para rentar una  pelicula")
    print("Digite 3 para devolver una pelicula")
    print("Digite 4 para Mostrar el catalogo de peliculas disponibles")
    print("Digite 5 para ***SALIR***")
    opcion = Correcion.validacion(Correcion)
    if(opcion==1):
        print("Cantidad(minimo 5):")
        n = Correcion.validacionC(Correcion)
        ipe = ImplementacionPilaEstatica(n)
        for i in range(n):
            print("Pelicula "+str(i+1)+":")
            titulo = str(input("Ingresa el titiulo:"))
            genero = str(input("Ingresa el enero:"))
            pelicula = Pelicula(titulo,genero)
            ipe.devolver(pelicula)
    elif(opcion==2):
        rentada = ipe.rentar()
        print(rentada)
    elif(opcion==3):
        ipe.devolver(rentada)
    elif(opcion==4):
        ipe.cantidad()
        ipe.mostrar()
    elif(opcion==5):
        print("Gracias por usar el programa")
        break
    else:
        print("Ups... Esta opcion es invalida")
                